public class Darat extends Hewan{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public int jumlahKaki;
    public boolean punyaSayap;

    public Darat(String cara, String nama, int kaki, boolean sayap){
        super(cara, nama, 2);
        this.jumlahKaki = kaki;
        this.punyaSayap = sayap;
    }

    public void showAnimalDetails(){        
        System.out.println("Hewan bergerak dengan cara : " + this.caraBergerak);
        System.out.println("Hewan ini punya nama : " + this.namaHewan);
        System.out.println("Hewan memiliki kaki sebanyak : " + this.jumlahKaki);
        System.out.println("Apa hewan ini punya sayap? : " + this.punyaSayap);
        System.out.println("Hewan ini adalah hewan laut");
    }
}