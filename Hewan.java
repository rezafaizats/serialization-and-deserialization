import java.io.Serializable;

public class Hewan implements Serializable
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public String caraBergerak;
    public String namaHewan;
    public int HewanID;

    public Hewan(String cara, String nama, int id){
        this.caraBergerak = cara;
        this.namaHewan = nama;
        this.HewanID = id;
    }

    public void showAnimalDetails(){
        System.out.println("Hewan bergerak dengan cara : " + this.caraBergerak);
        System.out.println("Hewan ini punya nama : " + this.namaHewan);
        System.out.println("Hewan ini tidak hidup di darat / di laut");
    }
}