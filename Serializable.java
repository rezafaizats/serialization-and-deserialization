import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serializable {

    public static void main(String[] args) {
        Hewan ikan = new Laut("berenang", "nemo", "insang", false);
        String filename = "file.ser";

        // Serialization
        try {
            // Save object into file
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);

            // Method
            out.writeObject(ikan);

            out.close();
            file.close();

            System.out.println("Object has been serialized.");
        }

        catch (IOException exception) {
            System.out.println("IOexcepetion caught!");
        }

        Hewan objek1 = null;
        // Deserialization
        try {
            // Reading object from file
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);

            // Deserialization method
            try {
                objek1 = (Hewan)in.readObject();

            } catch (ClassNotFoundException e) {
                System.out.println("Exception caught for deserialization.");
            }

            in.close();
            file.close();

            System.out.println("Object has been deserialized.");
            if(objek1 instanceof Laut){
                ((Laut) objek1).showAnimalDetails();
            }else if(objek1 instanceof Darat){
                ((Darat) objek1).showAnimalDetails();
            }else{
                objek1.showAnimalDetails();
            }
        }
        catch(IOException exception){
            System.out.println("Exception caught! " + exception);
        }
    }
}

