public class Laut extends Hewan{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public String caraBernapas;
    public boolean tempatHidup;

    public Laut(String cara, String nama, String bernapas, boolean habitat){
        super(cara, nama, 1);
        this.caraBernapas = bernapas;
        this.tempatHidup = habitat;
    }

    public void showAnimalDetails(){
        System.out.println("Hewan bergerak dengan cara : " + this.caraBergerak);
        System.out.println("Hewan ini punya nama : " + this.namaHewan);
        System.out.println("Hewan bernapas dengan : " + this.caraBernapas);
        System.out.println("Apa hewan ini hidup di air tawar? : " + this.tempatHidup);
        System.out.println("Hewan ini adalah hewan laut");
    }
}